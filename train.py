# -*- coding: utf-8 -*-
import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.optim as optim

torch.manual_seed(1)

def to_scalar(var):
    # returns a python float
    return var.view(-1).data.tolist()[0]

def argmax(vec):
    # return the argmax as a python int
    _, idx = torch.max(vec, 1)
    return to_scalar(idx)

def prepare_sequence(seq, to_ix):
    idxs = [to_ix[w] for w in seq]
    tensor = torch.LongTensor(idxs)
    return autograd.Variable(tensor)

def prepare_batch_sequence(seqs, to_ix):
    idxs = [[to_ix[w] for w in seq] for seq in seqs]
    tensor = torch.LongTensor(idxs)
    return autograd.Variable(tensor)

def decode_output(seq, ix_to):
    tags = [ix_to[w] for w in seq]
    return tags

# Compute log sum exp in a numerically stable way for the forward algorithm
def log_sum_exp(vec):
    max_score = vec[0, argmax(vec)]
    max_score_broadcast = max_score.view(1, -1).expand(1, vec.size()[1])
    return max_score + \
        torch.log(torch.sum(torch.exp(vec - max_score_broadcast)))

#####################################################################
# Create model

class BiLSTM_CRF(nn.Module):

    def __init__(self, vocab_size, tag_to_ix, embedding_dim, hidden_dim):
        super(BiLSTM_CRF, self).__init__()
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        self.vocab_size = vocab_size
        self.tag_to_ix = tag_to_ix
        self.tagset_size = len(tag_to_ix)

        self.char_embeds = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim // 2, num_layers=1, bidirectional=True)

        # Maps the output of the LSTM into tag space.
        self.hidden2tag = nn.Linear(hidden_dim, self.tagset_size)

        # Matrix of transition parameters.  Entry i,j is the score of
        # transitioning *to* i *from* j.
        self.transitions = nn.Parameter(
            torch.randn(self.tagset_size, self.tagset_size))

        self.hidden = self.init_hidden()

    def init_hidden(self):
        return (autograd.Variable(torch.randn(2, 1, self.hidden_dim // 2)),
                autograd.Variable(torch.randn(2, 1, self.hidden_dim // 2)))

    def _forward_alg(self, feats):
        # Do the forward algorithm to compute the partition function
        init_alphas = torch.Tensor(1, self.tagset_size).fill_(-10000.)
        # START_TAG has all of the score.
        init_alphas[0][self.tag_to_ix[START_TAG]] = 0.
        # Wrap in a variable so that we will get automatic backprop
        forward_var = autograd.Variable(init_alphas)
        # Iterate through the sentence
        for feat in feats:
            alphas_t = []  # The forward variables at this timestep
            for next_tag in range(self.tagset_size):
                # broadcast the emission score: it is the same regardless of
                # the previous tag
                emit_score = feat[next_tag].view(1, -1).expand(1, self.tagset_size)
                # the ith entry of trans_score is the score of transitioning to
                # next_tag from i
                trans_score = self.transitions[next_tag].view(1, -1)
                #print(self.transitions[next_tag].view(1,-1))
                # The ith entry of next_tag_var is the value for the
                # edge (i -> next_tag) before we do log-sum-exp
                next_tag_var = forward_var + trans_score + emit_score
                # The forward variable for this tag is log-sum-exp of all the
                # scores.
                alphas_t.append(log_sum_exp(next_tag_var))
            forward_var = torch.cat(alphas_t).view(1, -1) # alphas_t 5 variables => cat = 5 dimensional tensor => .view = tensor with size 1,5
        terminal_var = forward_var + self.transitions[self.tag_to_ix[STOP_TAG]]
        alpha = log_sum_exp(terminal_var)
        return alpha

    def _get_lstm_features(self, sentence):
        self.hidden = self.init_hidden()
        embeds = self.char_embeds(sentence).view(len(sentence), 1, -1)
        lstm_out, self.hidden = self.lstm(embeds, self.hidden)
        lstm_out = lstm_out.view(len(sentence), self.hidden_dim)
        lstm_feats = self.hidden2tag(lstm_out)
        return lstm_feats

    def _score_sentence(self, feats, tags):
        # Gives the score of a provided tag sequence
        score = autograd.Variable(torch.Tensor([0]))
        tags = torch.cat([torch.LongTensor([self.tag_to_ix[START_TAG]]), tags])
        for i, feat in enumerate(feats):
            score = score + \
                self.transitions[tags[i + 1], tags[i]] + feat[tags[i + 1]]
        score = score + self.transitions[self.tag_to_ix[STOP_TAG], tags[-1]]
        return score

    def _viterbi_decode(self, feats):
        backpointers = []

        # Initialize the viterbi variables in log space
        init_vvars = torch.Tensor(1, self.tagset_size).fill_(-10000.)
        init_vvars[0][self.tag_to_ix[START_TAG]] = 0

        # forward_var at step i holds the viterbi variables for step i-1
        forward_var = autograd.Variable(init_vvars)
        for feat in feats:
            bptrs_t = []  # holds the backpointers for this step
            viterbivars_t = []  # holds the viterbi variables for this step

            for next_tag in range(self.tagset_size):
                # next_tag_var[i] holds the viterbi variable for tag i at the
                # previous step, plus the score of transitioning
                # from tag i to next_tag.
                # We don't include the emission scores here because the max
                # does not depend on them (we add them in below)
                next_tag_var = forward_var + self.transitions[next_tag]
                best_tag_id = argmax(next_tag_var)
                bptrs_t.append(best_tag_id)
                viterbivars_t.append(next_tag_var[0][best_tag_id])
            # Now add in the emission scores, and assign forward_var to the set
            # of viterbi variables we just computed
            forward_var = (torch.cat(viterbivars_t) + feat).view(1, -1)
            backpointers.append(bptrs_t)

        # Transition to STOP_TAG
        terminal_var = forward_var + self.transitions[self.tag_to_ix[STOP_TAG]]
        best_tag_id = argmax(terminal_var)
        path_score = terminal_var[0][best_tag_id]

        # Follow the back pointers to decode the best path.
        best_path = [best_tag_id]
        for bptrs_t in reversed(backpointers):
            best_tag_id = bptrs_t[best_tag_id]
            best_path.append(best_tag_id)
        # Pop off the start tag (we dont want to return that to the caller)
        start = best_path.pop()
        assert start == self.tag_to_ix[START_TAG]  # Sanity check
        best_path.reverse()
        return path_score, best_path

    def neg_log_likelihood(self, sentence, tags):
        feats = self._get_lstm_features(sentence)
        forward_score = self._forward_alg(feats)
        gold_score = self._score_sentence(feats, tags)
        return forward_score - gold_score

    def forward(self, sentence):  # dont confuse this with _forward_alg above.
        # Get the emission scores from the BiLSTM
        lstm_feats = self._get_lstm_features(sentence)

        # Find the best path, given the features.
        score, tag_seq = self._viterbi_decode(lstm_feats)
        return score, tag_seq

#####################################################################
# Run training

START_TAG = "<START>"
STOP_TAG = "<STOP>"
EMBEDDING_DIM = 5
HIDDEN_DIM = 4

class TrainingData(object):
    """docstring for TrainingData"""
    def __init__(self):
        self.batch_idx = 0
        self.data = [(
                        list("Bulent Ecevit bugun Ankara'ya geldi."),
                        list('BBBBBBBBBBBBBOOOOOOOIIIIIIOOOOOOOOOO')
                    ), (
                        list("Mustafa kahve icmeyi cok seviyor"),
                        list('BBBBBBBOOOOOOOOOOOOOOOOOOOOOOOOO')
                    ), (
                        list("Merve de cok sever aslinda"),
                        list('BBBBBOOOOOOOOOOOOOOOOOOOOO')
                    ),(
                        list("Bulent Ecevit bugun Ankara'ya geldi."),
                        list('BBBBBBBBBBBBBOOOOOOOIIIIIIOOOOOOOOOO')
                    ), (
                        list("Mustafa kahve icmeyi cok seviyor"),
                        list('BBBBBBBOOOOOOOOOOOOOOOOOOOOOOOOO')
                    ), (
                        list("Merve de cok sever aslinda"),
                        list('BBBBBOOOOOOOOOOOOOOOOOOOOO')
                    )]
    def prepare_c2ix(self):
        char_to_ix = {}
        for sentence, tags in self.data:
            for char in sentence:
                if char not in char_to_ix:
                    char_to_ix[char] = len(char_to_ix)
        return char_to_ix

    def nextbatch(self, batch_size):
        if self.batch_idx + batch_size < len(self.data):
            self.batch_idx += batch_size
            return self.data[self.batch_idx:self.batch_idx+batch_size]
        else:
            self.batch_idx = 0
            return self.data[self.batch_idx:]


tag_to_ix = {"O": 0, "B": 1, "I": 2, START_TAG: 3, STOP_TAG: 4}
ix_to_tag = {0: "O", 1: "B", 2: "I"}
tr_data = TrainingData()
char_to_ix = tr_data.prepare_c2ix()

training_data = [(
                    list("Bulent Ecevit bugun Ankara'ya geldi."),
                    list('BBBBBBBBBBBBBOOOOOOOIIIIIIOOOOOOOOOO')
                ), (
                    list("Mustafa kahve icmeyi cok seviyor"),
                    list('BBBBBBBOOOOOOOOOOOOOOOOOOOOOOOOO')
                ), (
                    list("Merve de cok sever aslinda"),
                    list('BBBBBOOOOOOOOOOOOOOOOOOOOO')
                ),(
                    list("Bulent Ecevit bugun Ankara'ya geldi."),
                    list('BBBBBBBBBBBBBOOOOOOOIIIIIIOOOOOOOOOO')
                ), (
                    list("Mustafa kahve icmeyi cok seviyor"),
                    list('BBBBBBBOOOOOOOOOOOOOOOOOOOOOOOOO')
                ), (
                    list("Merve de cok sever aslinda"),
                    list('BBBBBOOOOOOOOOOOOOOOOOOOOO')
                )]

test_data = [(
    list("Merve ve Mustafa bugun Ankara'ya geldi."),
    list("BBBBBOOOOBBBBBBBOOOOOOOOOOOOOOOOOOOOOOO")
), (
    list("Bulent kahve icmeyi cok seviyor"),
    list("BBBBBBOOOOOOOOOOOOOOOOOOOOOOOOO")
), (
    list("Mustafa Ecevit de cok sever aslinda"),
    list("BBBBBBBBBBBBBBOOOOOOOOOOOOOOOOOOOOO")
)]

model = BiLSTM_CRF(len(char_to_ix), tag_to_ix, EMBEDDING_DIM, HIDDEN_DIM)
optimizer = optim.SGD(model.parameters(), lr=0.01, weight_decay=1e-4)

# # Check predictions before training
precheck_sent = prepare_sequence(training_data[1][0], char_to_ix)
precheck_tags = torch.LongTensor([tag_to_ix[t] for t in training_data[1][1]])

batch_size = 2

# Make sure prepare_sequence from earlier in the LSTM section is loaded
print("Training started")
for epoch in range(50):  # again, normally you would NOT do 300 epochs, it is toy data
    for sentence, tags in training_data:
        # Step 1. Remember that Pytorch accumulates gradients.
        # We need to clear them out before each instance
        model.zero_grad()

        # Step 2. Get our inputs ready for the network, that is,
        # turn them into Variables of char indices.
        sentence_in = prepare_sequence(sentence, char_to_ix)
        targets = torch.LongTensor([tag_to_ix[t] for t in tags])

        # Step 3. Run our forward pass.
        neg_log_likelihood = model.neg_log_likelihood(sentence_in, targets)

        # Step 4. Compute the loss, gradients, and update the parameters by
        # calling optimizer.step()
        neg_log_likelihood.backward()
        optimizer.step()
    if epoch % 50 == 0:
        print(epoch)
        print(training_data[1][0])
        print(decode_output(model(precheck_sent)[1], ix_to_tag))

torch.save(model.state_dict(),"saved.model")
#del(model)
#model = BiLSTM_CRF(len(char_to_ix), tag_to_ix, EMBEDDING_DIM, HIDDEN_DIM)
#model.load_state_dict(torch.load("saved.model"))

# Check predictions after training
for i in range(len(test_data)):
    precheck_sent = prepare_sequence(test_data[i][0], char_to_ix)
    print("".join(test_data[i][0]))
    print("".join(decode_output(model(precheck_sent)[1], ix_to_tag)))
    print("\n")
# We got it!


######################################################################
# Exercise: A new loss function for discriminative tagging
# --------------------------------------------------------
#
# It wasn't really necessary for us to create a computation graph when
# doing decoding, since we do not backpropagate from the viterbi path
# score. Since we have it anyway, try training the tagger where the loss
# function is the difference between the Viterbi path score and the score
# of the gold-standard path. It should be clear that this function is
# non-negative and 0 when the predicted tag sequence is the correct tag
# sequence. This is essentially *structured perceptron*.
#
# This modification should be short, since Viterbi and score\_sentence are
# already implemented. This is an example of the shape of the computation
# graph *depending on the training instance*. Although I haven't tried
# implementing this in a static toolkit, I imagine that it is possible but
# much less straightforward.
#
# Pick up some real data and do a comparison!
#
