import json

class Lang:
    def __init__(self, name):
        self.name = name
        self.word2index = {}
        self.word2count = {}
        self.index2word = {0: "SOS", 1: "EOS"}
        self.n_words = 2  # Count SOS and EOS

    def addSentence(self, sentence):
        for word in sentence.split(' '):
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1

    def save(self, filename):
        with open(filename,"w") as ofile:
            ofile.write(json.dumps({"name":self.name,"w2i":self.word2index,"w2c":self.word2count, "i2w":self.index2word}))
        print("Language successfully written into %s"%filename)

    def load(self, filename):
        with open(filename,"r") as ofile:
            loaded = json.loads(ofile.read())
            self.name = loaded["name"]
            self.word2index = loaded["w2i"]
            self.word2count = loaded["w2c"]
            i2w = loaded["i2w"]
            self.index2word = {int(k):v for k,v in i2w.items()}
        self.n_words = len(self.index2word)
        print("Model successfully loaded.")