FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
    python3-pip \
    wget \
    python3 \
    python3-numpy

RUN pip3 install --upgrade pip

# Add and install Python modules
ADD requirements.txt /src/requirements.txt
RUN cd /src; pip3 install -r requirements.txt
RUN cd /src; wget http://download.pytorch.org/whl/cu75/torch-0.1.12.post2-cp35-cp35m-linux_x86_64.whl
RUN cd /src; pip3 install torch-0.1.12.post2-cp35-cp35m-linux_x86_64.whl

# Bundle app source
ADD . /src

# Expose
EXPOSE  80

# Run
CMD ["python3", "/src/app.py"]
